package de.jtrx.elastictest;

import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "de.jtrx.elastictest.repositories")
@ComponentScan(basePackages = { "de.jtrx.elastictest" })
public class ElasticsearchClientConfig extends AbstractElasticsearchConfiguration {

    @Value("${ELASTICSEARCH_HOST}")
    private String host;

    @Override
    @Bean
    public RestHighLevelClient elasticsearchClient() {
        final ClientConfiguration clientConfiguration =
                ClientConfiguration
                        .builder()
                        .connectedTo(host)
//                        .withBasicAuth("elastic", "myPassword")
                        .build();

        return RestClients
                .create(clientConfiguration)
                .rest();
    }

    @Bean
    public RestClient restClient() {
        final ClientConfiguration clientConfiguration =
                ClientConfiguration
                        .builder()
                        .connectedTo(host)
//                        .withBasicAuth("elastic", "myPassword")
                        .build();

        return RestClients
                .create(clientConfiguration)
                .lowLevelRest();
    }
}
