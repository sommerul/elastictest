#FROM gradle:7.0.2-jdk11
#COPY --chown=gradle:gradle . /project
#WORKDIR /project
#RUN gradle build -x test --no-daemon
#COPY build/libs/elastictest-0.0.1-SNAPSHOT.jar /elastictest.jar

#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/elastictest.jar"]



#FROM openjdk:11-jdk
#
#COPY . /project
#WORKDIR /project
#
#RUN ./gradlew clean build -x test
#RUN cp build/libs/elastictest-0.0.1-SNAPSHOT.jar /elastictest.jar
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/elastictest.jar"]


ARG GRADLE_IMAGE=gradle:7.0.2-jdk11
ARG JRE_IMAGE=openjdk:11-jre-slim
ARG APP_DIR=/app
ARG GRADLE_CACHE_DIR=/cache

# gradle cache docker layer
FROM $GRADLE_IMAGE AS cache

ARG GRADLE_CACHE_DIR
WORKDIR $GRADLE_CACHE_DIR/
VOLUME $GRADLE_CACHE_DIR/
ENV GRADLE_USER_HOME=$GRADLE_CACHE_DIR/

COPY build.gradle settings.gradle $GRADLE_CACHE_DIR/

RUN gradle --no-daemon dependencies --stacktrace


# development/test/ci image
FROM $GRADLE_IMAGE AS development

ARG APP_DIR
WORKDIR $APP_DIR/

ARG GRADLE_CACHE_DIR
COPY --from=cache $GRADLE_CACHE_DIR /home/gradle/.gradle

COPY . $APP_DIR/

RUN gradle --parallel clean build -x test

# production image
FROM $JRE_IMAGE AS production

ARG MYUSER=app
ARG MYGROUP=app
ARG MYUID=1000
ARG MYGID=1000

RUN set -e \
    #
    # Add a runtime user
    && groupadd --system --gid $MYGID $MYGROUP \
    && useradd --system --gid $MYGROUP --uid $MYUID $MYUSER

ARG APP_DIR
WORKDIR $APP_DIR/
RUN chown $MYUSER:$MYGROUP $APP_DIR/
USER $MYUSER:$MYGROUP

COPY --from=development --chown=$MYUSER:$MYGROUP $APP_DIR/build/libs/elastictest-0.0.1-SNAPSHOT.jar $APP_DIR/elastictest.jar

ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "elastictest.jar"]
